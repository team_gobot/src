/* 
  Line Follow with QTIs.c
*/

#include "simpletools.h"                       // Include libraries
#include "abdrive.h"
#include "servo.h"

int getQTIs(int highPin, int lowPin);          // Forward declare function

int main()                                     // Main function
{
  int item;
int left = 0;
int leftPrev = 0;
int right = 0;
int rightPrev = 0;
int driveLeft, driveRight;
int scale = 1000;
  
 
  while(1)                                     // Main loop
  {
    
    int qtis = getQTIs(7, 4);                  // Check stripe position
 switch (qtis)
    {
     
      case 0b1000:
      servo_set(13, 375);
      servo_set(12, 0);
        break;
      case 0b1100:
      servo_set(13, 750);
      servo_set(12, 375);
        break;
      case 0b1110:
      servo_set(13, 1078);
      servo_set(12, 750);
        break;
      case 0b0100:
      servo_set(12, 1500);
      servo_set(13, 1078);
        break;
      case 0b0110:
      servo_set(13, 1500);
      servo_set(12, 1500);
        left = 64;
        right = 64;
        break;
      case 0b0010:
      servo_set(13, 1078);
      servo_set(12, 1500);
        break;
      case 0b0111:
      servo_set(13, 750);
      servo_set(12, 1078);
        break;
      case 0b0011:
      servo_set(13, 375);
      servo_set(12, 750);
  
        break;
      case 0b0001:
      servo_set(13, 0);
      servo_set(12, 375);
        
        break;
      default:
      servo_set(13, 0);
      servo_set(12, 0);             
    }
   
   
    pause(20);
  }
}
int getQTIs(int highPin, int lowPin)           // Function - getQTIs
{
  int dt = (CLKFREQ / 1000000) * 230;          // Set up 230 us time increment
  set_outputs(highPin, lowPin, 0b1111);        // highPin...lowPin set high
  set_directions(highPin, lowPin, 0b1111);     // highPin...lowPin set to output
  waitcnt(dt + CNT);                           // Wait 230 us
  set_directions(highPin, lowPin, 0b0000);     // highPin...lowPin st to input
  waitcnt(dt + CNT);                           // Wait 230 us
  int qtis = get_states(highPin, lowPin);      // Get 4-bit pattern QTIs apply 
  return qtis;                                 // Return val containing pattern
}